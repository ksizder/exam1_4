import time
def multiply_matrix():
    str1 = int(input("Enter the string count of first Matrix"))
    stlb1 = int(input("Enter the column count of first Matrix"))
    Matrix1 = [[int(input('Enter the element ')) for x in range(stlb1)] for y in range(str1)]
    str2 = int(input("Enter the string count of second Matrix"))
    stlb2 = int(input("Enter the column count of second Matrix"))
    Matrix2 = [[int(input('Enter the element ')) for x in range(stlb2)] for y in range(str2)]
    if stlb1 != str2:
        return
    Matrix3 = [[0 for x in range(stlb2)] for y in range(str1)]
    for t_str in range(str1):
        for t_stlb in range(stlb2):
            tempsum = 0
            for i in range(str2):
                tempsum = tempsum + Matrix1[t_str][i] * Matrix2[i][t_stlb]
            Matrix3[t_str][t_stlb] = tempsum;
    return Matrix3

def test_matrix():
    for counter in range(1, 11):
        str1 = 100 * counter
        stlb1 = 100
        Matrix1 = [[0 for x in range(stlb1)] for y in range(str1)]
        str2 = 100
        stlb2 = 100 * counter
        Matrix2 = [[0 for x in range(stlb2)] for y in range(str2)]
        if stlb1 != str2:
             return
        Matrix3 = [[0 for x in range(stlb2)] for y in range(str1)]
        start_time = time.time()
        for t_str in range(str1):
             for t_stlb in range(stlb2):
                tempsum = 0
                for i in range(str2):
                    tempsum = tempsum + Matrix1[t_str][i] * Matrix2[i][t_stlb]
                Matrix3[t_str][t_stlb] = tempsum
        print("--- %s seconds ---" % (time.time() - start_time))


